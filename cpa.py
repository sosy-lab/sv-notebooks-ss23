import ast
import copy
import itertools
import astunparse
from graphviz import Digraph
from enum import Enum


class InstructionType(Enum):
    STATEMENT = 1
    ASSUMPTION = 2


class Instruction:
    """An instruction is either an assignment or an assumption"""

    def __init__(self, expression, kind=InstructionType.STATEMENT, negated=False):
        self.kind = kind
        self.expression = expression
        self.negated = negated  # we might need this information at some point

    @staticmethod
    def assumption(expression, negated=False):
        if negated:
            expression = ast.UnaryOp(op=ast.Not(), operand=expression)
        return Instruction(expression, kind=InstructionType.ASSUMPTION, negated=negated)

    @staticmethod
    def statement(expression):
        return Instruction(expression)


class CFANode:
    index = 0

    def __init__(self):
        self.node_id = CFANode.index
        self.entering_edges = list()
        self.leaving_edges = list()
        CFANode.index += 1

    def __str__(self):
        return "(%s)" % str(self.node_id)

    @staticmethod
    def merge(a, b):
        for entering_edge in b.entering_edges:
            entering_edge.successor = a
            a.entering_edges.append(entering_edge)
        for (
            leaving_edge
        ) in (
            b.leaving_edges
        ):  # might not be needed even since we merge only sucessorless b-nodes
            leaving_edge.predecessor = a
            a.leaving_edges.append(leaving_edge)
        b.entering_edges = list()
        b.leaving_edges = list()
        if CFANode.index == b.node_id + 1:
            CFANode.index -= 1
        return a


class CFAEdge:
    def __init__(self, predecessor, successor, instruction):
        self.predecessor = predecessor
        self.successor = successor
        predecessor.leaving_edges.append(self)
        successor.entering_edges.append(self)
        self.instruction = instruction

    def __str__(self):
        return "%s -%s-> %s" % (
            str(self.predecessor),
            self.label(),
            str(self.successor),
        )

    def label(self):
        return astunparse.unparse(self.instruction.expression).strip()


class CFACreator(ast.NodeVisitor):
    def __init__(self):
        self.root = CFANode()
        self.node_stack = list()
        self.node_stack.append(self.root)

    def visit_While(
        self, node
    ):  # TODO: break and continue have to be considered inside loops
        entry_node = self.node_stack.pop()
        inside = CFANode()
        edge = CFAEdge(entry_node, inside, Instruction.assumption(node.test))
        outside = CFANode()
        edge = CFAEdge(
            entry_node, outside, Instruction.assumption(node.test, negated=True)
        )
        self.node_stack.append(inside)
        for statement in node.body:
            self.visit(statement)
        body_exit_node = self.node_stack.pop()
        CFANode.merge(entry_node, body_exit_node)
        self.node_stack.append(outside)

    def visit_Break(self, node):
        self._nop(node)  # TODO: add proper implementation for break

    def visit_Continue(self, node):
        self._nop(node)  # TODO: add proper implementation for continue

    def _nop(self, node):
        entry_node = self.node_stack.pop()
        to = CFANode()
        edge = CFAEdge(entry_node, to, Instruction.statement(node))
        self.node_stack.append(to)
        self.node_stack.append(to)

    def visit_If(self, node):
        entry_node = self.node_stack.pop()
        left = CFANode()
        edge = CFAEdge(entry_node, left, Instruction.assumption(node.test))
        right = CFANode()
        edge = CFAEdge(
            entry_node, right, Instruction.assumption(node.test, negated=True)
        )
        self.node_stack.append(left)
        for statement in node.body:
            self.visit(statement)
        left_exit = self.node_stack.pop()
        self.node_stack.append(right)
        for statement in node.orelse:
            self.visit(statement)
        right_exit = self.node_stack.pop()
        merged_exit = CFANode.merge(left_exit, right_exit)
        self.node_stack.append(merged_exit)

    def visit_Expr(self, node):
        entry_node = self.node_stack.pop()
        exit_node = CFANode()
        edge = CFAEdge(entry_node, exit_node, Instruction.statement(node.value))
        self.node_stack.append(exit_node)

    def visit_Assign(self, node):
        entry_node = self.node_stack.pop()
        exit_node = CFANode()
        edge = CFAEdge(entry_node, exit_node, Instruction.statement(node))
        self.node_stack.append(exit_node)


class Graphable:
    def get_node_label(self):
        pass

    def get_edge_labels(self, other):
        pass

    def get_successors(self):
        pass


class GraphableCFANode(Graphable):
    def __init__(self, node):
        assert isinstance(node, CFANode)
        self.node = node

    def get_node_label(self):
        return str(self.node.node_id)

    def get_edge_labels(self, other):
        return [
            edge.label()
            for edge in self.node.leaving_edges
            if edge.successor == other.node
        ]

    def get_successors(self):
        return [GraphableCFANode(edge.successor) for edge in self.node.leaving_edges]

    def __eq__(self, other):
        return self.node == other.node

    def __hash__(self):
        return self.node.__hash__()


def graphable_to_dot(root, nodeattrs={"shape": "circle"}):
    dot = Digraph()
    for (key, value) in nodeattrs.items():
        dot.attr("node", [(key, value)])
    dot.node(root.get_node_label())
    waitlist = set()
    waitlist.add(root)
    reached = set()
    reached.add(root)
    while not len(waitlist) == 0:
        node = waitlist.pop()
        for successor in node.get_successors():
            for edgelabel in node.get_edge_labels(successor):
                dot.edge(node.get_node_label(), successor.get_node_label(), edgelabel)
            if not successor in reached:
                waitlist.add(successor)
                reached.add(successor)
                dot.node(successor.get_node_label())
    return dot


class AbstractState(object):
    pass


class CPA:
    def get_initial_state(self):
        raise NotImplementedError("get_initial_state not implemented!")

    def get_transfer_relation(self):
        raise NotImplementedError("get_transfer_relation not implemented!")

    def get_merge_operator(self):
        raise NotImplementedError("get_merge_operator not implemented!")

    def get_stop_operator(self):
        raise NotImplementedError("get_stop_operator not implemented!")


class TransferRelation:
    def get_abstract_successors(self, predecessor):
        raise NotImplementedError("get_abstract_successors not implemented!")

    def get_abstract_successors_for_edge(self, predecessor, edge):
        raise NotImplementedError("get_abstract_successors_for_edge not implemented!")


class StopOperator:
    def stop(self, state, reached):
        raise NotImplementedError("stop not implemented!")


class StopSepOperator:
    def __init__(self, subsumes):
        self.subsumes = subsumes

    def stop(self, state, reached):
        return [other for other in reached if self.subsumes(state, other)]


class MergeOperator:
    def merge(self, state1, state2):
        raise NotImplementedError("merge not implemented!")


class MergeSepOperator(MergeOperator):
    def merge(self, state1, state2):
        return state2


class LocationState(AbstractState):
    def __init__(self, node):
        self.location = node

    def __str__(self):
        return "@%s" % self.location.node_id

    def __eq__(self, other):
        return self.location == other.location

    def __hash__(self):
        return self.location.__hash__()


class LocationTransferRelation(TransferRelation):
    def get_abstract_successors(self, predecessor):
        return [
            LocationState(edge.successor) for edge in predecessor.location.leaving_edges
        ]

    def get_abstract_successors_for_edge(self, predecessor, edge):
        return [LocationState(edge.successor)]


class LocationStopOperator(StopOperator):
    def stop(self, e, reached):
        return set(eprime for eprime in reached if e.location == eprime.location)


class LocationCPA(CPA):
    def __init__(self, cfa_root):
        self.root = cfa_root

    def get_initial_state(self):
        return LocationState(self.root)

    def get_stop_operator(self):
        return LocationStopOperator()

    def get_merge_operator(self):
        return MergeSepOperator()

    def get_transfer_relation(self):
        return LocationTransferRelation()


class ARGState(AbstractState):
    index = 0

    def __init__(self, wrapped_state, parent=None):
        self.wrapped_state = wrapped_state
        self.state_id = ARGState.index
        ARGState.index += 1
        self.parents = set()
        if parent:
            self.parents.add(parent)
            parent.children.add(self)
        self.children = set()

    def __str__(self):
        return f"N{self.state_id} - {self.wrapped_state}"

    def is_target(self):
        return (
            hasattr(self.wrapped_state, "is_target") and self.wrapped_state.is_target()
        )


class ARGTransferRelation(TransferRelation):
    def __init__(self, wrapped_transfer_relation):
        self.wrapped_transfer_relation = wrapped_transfer_relation

    def get_abstract_successors(self, predecessor):
        return [
            ARGState(wrapped_successor, predecessor)
            for wrapped_successor in self.wrapped_transfer_relation.get_abstract_successors(
                predecessor.wrapped_state
            )
        ]


class ARGStopOperator(StopOperator):
    def __init__(self, wrapped_stop_operator):
        self.wrapped_stop_operator = wrapped_stop_operator

    def stop(self, e, reached):
        return self.wrapped_stop_operator.stop(
            e.wrapped_state, [eprime.wrapped_state for eprime in reached]
        )


class ARGMergeOperator(MergeOperator):
    def __init__(self, wrapped_merge_operator):
        self.wrapped_merge_operator = wrapped_merge_operator

    def merge(self, state1, state2):
        wrapped_state1 = state1.wrapped_state
        wrapped_state2 = state2.wrapped_state
        merge_result = self.wrapped_merge_operator.merge(wrapped_state1, wrapped_state2)
        if (
            merge_result == wrapped_state2
        ):  # and (wrapped_state1 != wrapped_state2 or all(parent1 in state2.parents for parent1 in state1.parents)):
            return state2
        else:
            # merge both into a new state:
            parents = state1.parents.union(state2.parents)
            children = state1.children.union(state2.children)
            new_state = ARGState(merge_result)
            for state in (state1, state2):
                for parent in state.parents:
                    parent.children.discard(state)
                    parent.children.add(new_state)
                state.parents = set()
                for child in state.children:
                    child.parents.discard(state)
                    child.parents.add(new_state)
                state.children = set()
            new_state.children = children
            new_state.parents = parents
            return new_state


class ARGCPA(CPA):
    def __init__(self, wrapped_cpa):
        self.wrapped_cpa = wrapped_cpa

    def get_initial_state(self):
        return ARGState(self.wrapped_cpa.get_initial_state())

    def get_stop_operator(self):
        return ARGStopOperator(self.wrapped_cpa.get_stop_operator())

    def get_merge_operator(self):
        return ARGMergeOperator(self.wrapped_cpa.get_merge_operator())

    def get_transfer_relation(self):
        return ARGTransferRelation(self.wrapped_cpa.get_transfer_relation())


class GraphableARGState(Graphable):
    def __init__(self, arg_state):
        assert isinstance(arg_state, ARGState)
        self.arg_state = arg_state

    def get_node_label(self):
        return str("N%d\n%s" % (self.arg_state.state_id, self.arg_state.wrapped_state))

    def get_edge_labels(self, other):
        loc1 = self._extract_location(self)
        loc2 = self._extract_location(other)
        if loc1 and loc2:
            for leaving_edge in loc1.leaving_edges:
                if leaving_edge.successor == loc2:
                    return [leaving_edge.label()]
        return [""]

    def _extract_location(self, state):
        waitlist = set()
        waitlist.add(state.arg_state)
        location = None
        while waitlist:
            current = waitlist.pop()
            if isinstance(current, LocationState):
                location = current.location
                break
            if hasattr(current, "wrapped_state"):
                waitlist.add(current.wrapped_state)
            elif hasattr(current, "wrapped_states"):
                waitlist.update(current.wrapped_states)
        return location

    def get_successors(self):
        return [GraphableARGState(child) for child in self.arg_state.children]

    def __eq__(self, other):
        return self.arg_state == other.arg_state

    def __hash__(self):
        return self.arg_state.__hash__()


class MCAlgorithm:
    def __init__(self, cpa):
        self.cpa = cpa

    def run(self, reached, waitlist):
        transfer_relation = self.cpa.get_transfer_relation()
        merge_operator = self.cpa.get_merge_operator()
        stop_operator = self.cpa.get_stop_operator()
        while len(waitlist) != 0:
            current_state = waitlist.pop()
            for eprime in transfer_relation.get_abstract_successors(current_state):
                if not stop_operator.stop(eprime, reached):
                    waitlist.add(eprime)
                    reached.add(eprime)
        return (reached, waitlist)


class CompositeState(AbstractState):
    def __init__(self, wrapped_states):
        self.wrapped_states = wrapped_states

    def is_target(self):
        return any(
            [
                hasattr(state, "is_target") and state.is_target()
                for state in self.wrapped_states
            ]
        )

    def __eq__(self, other):
        if other is self:
            return True
        if len(self.wrapped_states) != len(other.wrapped_states):
            return False
        return all(a == b for (a, b) in zip(self.wrapped_states, other.wrapped_states))

    def __hash__(self):
        return tuple(
            wrapped_state.__hash__ for wrapped_state in self.wrapped_states
        ).__hash__()

    def __str__(self):
        return "|%s|" % "|\n|".join([str(state) for state in self.wrapped_states])


class CompositeStopOperator(AbstractState):
    def __init__(self, wrapped_stop_operators):
        self.wrapped_stop_operators = wrapped_stop_operators

    def stop(self, e, reached):
        for i in range(len(self.wrapped_stop_operators)):
            stop_operator = self.wrapped_stop_operators[i]
            state = e.wrapped_states[i]
            projreached = set(eprime.wrapped_states[i] for eprime in reached)
            partialresult = stop_operator.stop(state, projreached)
            if not partialresult:
                return set()
            reached = [s for s in reached if s.wrapped_states[i] in partialresult]
        return reached


class CompositeTransferRelation(TransferRelation):
    def __init__(self, wrapped_transfer_relations):
        self.wrapped_transfer_relations = wrapped_transfer_relations

    def get_abstract_successors(self, predecessor):
        location_states = [
            state
            for state in predecessor.wrapped_states
            if isinstance(state, LocationState)
        ]
        if len(location_states) == 0:
            return [
                CompositeState(product)
                for product in itertools.product(
                    *[
                        transfer.get_abstract_successors(state)
                        for (transfer, state) in zip(
                            self.wrapped_transfer_relations, predecessor.wrapped_states
                        )
                    ]
                )
            ]
        else:
            location_state = location_states[0]
            result = list()
            for edge in location_state.location.leaving_edges:
                result += self.get_abstract_successors_for_edge(predecessor, edge)
            return result

    def get_abstract_successors_for_edge(self, predecessor, edge):
        return [
            CompositeState(product)
            for product in itertools.product(
                *[
                    transfer.get_abstract_successors_for_edge(state, edge)
                    for (transfer, state) in zip(
                        self.wrapped_transfer_relations, predecessor.wrapped_states
                    )
                ]
            )
        ]


class CompositeMergeOperator(MergeOperator):
    def __init__(self, wrapped_merge_operators):
        self.wrapped_merge_operators = wrapped_merge_operators

    def merge(self, state1, state2):
        merge_results = list()
        wrapped_states1 = state1.wrapped_states
        wrapped_states2 = state2.wrapped_states
        all_changed = True
        for s1, s2, merge_operator in zip(
            wrapped_states1, wrapped_states2, self.wrapped_merge_operators
        ):
            merge_result = merge_operator.merge(s1, s2)
            if merge_result == s2 and s1 != s2:
                all_changed = False
                break
            merge_results.append(merge_result)
        if not all_changed:
            return state2
        else:
            return CompositeState(merge_results)


class CompositeCPA(CPA):
    def __init__(self, wrapped_cpas):
        self.wrapped_cpas = wrapped_cpas

    def get_initial_state(self):
        return CompositeState(
            [wrapped_cpa.get_initial_state() for wrapped_cpa in self.wrapped_cpas]
        )

    def get_stop_operator(self):
        return CompositeStopOperator(
            [wrapped_cpa.get_stop_operator() for wrapped_cpa in self.wrapped_cpas]
        )

    def get_merge_operator(self):
        return CompositeMergeOperator(
            [cpa.get_merge_operator() for cpa in self.wrapped_cpas]
        )

    def get_transfer_relation(self):
        return CompositeTransferRelation(
            [wrapped_cpa.get_transfer_relation() for wrapped_cpa in self.wrapped_cpas]
        )
