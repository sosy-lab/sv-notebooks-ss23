# SV Notebooks

Notebooks for software verification.

## Setup

We use Python 3.10 (any other recent Python version should be fine as well).
Please install this according to your OS.

On Linux, it may be useful to use a *virtual environment*
in order to be able to install Python packages as user
without affecting other Python software:

```bash
# One-time setup:
python3 -m venv ~/venv-sv-notebooks  # or any other directory
# Before each use:
. ~/venv-sv-notebooks/bin/activate # do not forget the dot command!
```

Then install requirements with (one-time setup):

```bash
pip install -r requirements.txt
```

Install an SMT solver Z3 for PySMT with (one-time setup):

```bash
pysmt-install --z3
```

To leave the virtual environment:
```bash
deactivate
```

## Start

Start a Jupyter notebook server in the virtual environment with:

```bash
jupyter notebook
```
